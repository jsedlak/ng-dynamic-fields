(function(angular) {
    'use strict';
    var myApp = angular.module('dynamicApp', ['ng']);

    myApp
        .config(function ($sceProvider) {
            $sceProvider.enabled(false);
        })
        .controller('DynamicFieldsController', ['$scope', function ($scope, $compile) {
            $scope.getTemplate = function (type) {
                return 'templates/' + type + '.htm';
            };
            
            $scope.entity = {
                title: '',
                description: ''
            };

            $scope.fields = [
                {
                    name: 'Title',
                    key: 'title',
                    type: 'singleline-text',
                    __system: {
                        label: 'Item Title',
                        placeholder: 'Give your todo a title'
                    },
                    binding: 'title'
                },
                {
                    name: 'Item Description',
                    key: 'description',
                    type: 'multiline-text',
                    __system: {
                        label: 'Description',
                        placeholder: 'Tell us what you want to do'
                    },
                    binding: 'description'
                }
            ];
        }]);
})(window.angular);
