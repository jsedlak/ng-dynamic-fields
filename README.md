# Dynamic Fields Demo

This is a simple repository demoing the notion of dynamic inputs, or fields that are unknown at compile time and driven by data. While simple, the goal is that an entity being edited within a database may have properties that are added by a user and thus unknown during development. This code is the basis for supporting such a requirement.

# Demo Site

A demo of this repository is available here:
https://jsedlak.gitlab.io/ng-dynamic-fields/

## Built On

* Angular 1.x
* Bootstrap 4.x